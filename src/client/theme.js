/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {createTheme} from "@mui/material/styles";

const white = "#ffffff";
const blue = "#2b3b46";
const orange = "#f29e60";
const grey = "#fafafa";
const linkColor = "#b2adb5";

export const theme = createTheme({
  palette: {
    primary: {
      main: blue
    },
    secondary: {
      main: blue,
      contrastText: white
    },
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
    text: {
      emptyHint: "#474747"
    },
    background: {
      dark: "#F4F6F8",
      default: grey
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 700,
      lg: 1080,
      xl: 1920
    }
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        a: {
          color: linkColor
        }
      }
    },
    MuiAppBar: {
      styleOverrides: {
        colorPrimary: {
          backgroundColor: "#FFFFFF",
          color: blue
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          color: linkColor,
          textDecoration: "none"
        }
      }
    },
    MuiIconButton: {
      styleOverrides: {
        colorPrimary: {
          color: orange
        },
        colorInherit: {
          color: blue
        }
      }
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          fontSize: "0.875rem",
          lineHeight: 1.43
        }
      }
    },
    MuiInputLabel: {
      styleOverrides: {
        shrink: {
          transform: "translate(0, -2px)",
          fontSize: "0.75rem",
          color: "rgba(0, 0, 0, 0.6) !important"
        },
        outlined:{
          transform: "translate(10px, -5px)",
        }
      }
    },
    MuiButtonBase: {
      styleOverrides: {
        root: {
          color: blue,
          "&.Mui-selected": {
            color: blue
          }
        },
        colorInherit: {
          color: white
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        containedPrimary: {
          backgroundColor: blue
        },
        textSecondary: {
          color: orange
        },
        containedSecondary: {
          color: white,
          backgroundColor: orange
        }
      }
    }
  }
});
/*

 
    MuiTab: {
      styleOverrides: {
        root: {
          '&.Mui-selected': {
            color: blue
          }
        }
      }
    }
    */
