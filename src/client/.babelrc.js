module.exports = function (api) {
  api.cache(false);

  const presets = [
    [
      "@babel/preset-env",
      {
        modules: false,
        shippedProposals: true,
        bugfixes: true
      }
    ],
    [
      "@babel/preset-react",
      {
        runtime: "automatic"
      }
    ]
  ];

  const plugins = [
    ["@babel/plugin-syntax-dynamic-import"],
    ["jsx-control-statements"],
    [
      "babel-plugin-import",
      {
        "libraryName": "@mui/material",
        "libraryDirectory": "",
        "camel2DashComponentName": false
      },
      "core"
    ],
    [
      "babel-plugin-import",
      {
        "libraryName": "@mui/icons-material",
        "libraryDirectory": "",
        "camel2DashComponentName": false
      },
      "icons"
    ],
    [
      "babel-plugin-import",
      {
        libraryName: "@mnemotix/weever-core",
        libraryDirectory: ".",
        camel2DashComponentName: false
      },
      "weever-core"
    ],
    [
      "babel-plugin-import",
      {
        libraryName: "lodash",
        libraryDirectory: ".",
        camel2DashComponentName: false
      },
      "lodash"
    ]
  ];

  return {
    presets,
    plugins,
    sourceMaps: "inline"
  };
};
